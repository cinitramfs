#ifndef ULINUX_ARCH_SIGNAL_SIGINFO_H
#define ULINUX_ARCH_SIGNAL_SIGINFO_H
//******************************************************************************
//*this code is protected by the GNU affero GPLv3
//*author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
//*                        <digital.ragnarok AT gmail dot com>
//******************************************************************************
union ulinux_sigval{
  ulinux_i i;
  void* ptr;
};

#define ULINUX_SI_PREAMBLE_SIZE (4*sizeof(ulinux_i))
#define ULINUX_SI_MAX_SIZE	128
#define ULINUX_SI_PAD_SIZE	((ULINUX_SI_MAX_SIZE-ULINUX_SI_PREAMBLE_SIZE)\
/sizeof(ulinux_i))

struct ulinux_siginfo{
  ulinux_i si_signo;
  ulinux_i si_errno;
  ulinux_i si_code;

  union{
    ulinux_i pad[ULINUX_SI_PAD_SIZE];

    //kill()
    struct{
      ulinux_i pid;//sender's pid 
      ulinux_u uid;//sender's uid
    } kill;

    //posix.1b timers
    struct{
      ulinux_i tid;//timer id
      ulinux_i overrun;//overrun count
      union ulinux_sigval sigval;//same as below
      ulinux_i sys_private;//not to be passed to user
    } timer;

    //posix.1b signals
    struct{
      ulinux_i pid;//sender's pid
      ulinux_u uid;//sender's uid
      union ulinux_sigval sigval;
    } rt;

    //SIGCHLD
    struct{
      ulinux_i pid;//which child
      ulinux_u uid;//sender's uid
      ulinux_i status;//exit code
      ulinux_ll utime __attribute__((aligned(4));
      ulinux_ll stime __attribute__((aligned(4));
    } sigchld;

    //SIGILL, SIGFPE, SIGSEGV, SIGBUS
    struct{
      void* addr;//faulting insn/memory ref.
      ulinux_s addr_lsb;//lsb of the reported address
    } sigfault;

    //SIGPOLL
    struct{
      ulinux_l band;//POLL_IN, POLL_OUT, POLL_MSG
      ulinux_i fd;
    } sigpoll;
  } fields;
};
#endif

