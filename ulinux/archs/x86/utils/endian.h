#ifndef ULINUX_ARCH_UTILS_ENDIAN_H
#define ULINUX_ARCH_UTILS_ENDIAN_H
//******************************************************************************
//*this code is protected by the GNU affero GPLv3
//*author:Sylvain BERTRAND (sylvain.bertrand AT gmail dot com)
//*                        <digital.ragnarok AT gmail dot com>
//******************************************************************************
static inline ulinux_u16 ulinux_cpu2be16(ulinx_u16 v)
{
  return (ulinux_u16)((((ulinux_u16)(v)&(ulinux_u16)0x00ffU)<<8)
                                   |(((ulinux_u16)(v)&(ulinux_u16)0xff00U)>>8));
}
#define ulinux_be162cpu(v) ulinux_cpu2be16(v)

static inline ulinux_u32 ulinux_cpu2be32(ulinux_u32 v)
{
  asm("bswapl %0":"=r" (v):"0" (v));
  return v;
}
#define ulinux_be322cpu(v) ulinux_cpu2be32(v)

static inline ulinux_u64 ulinux_cpu2be64(ulinux_u64 v)
{
  asm("bswapq %0":"=r" (v):"0" (v));
  return v;
}
#define ulinux_be642cpu(v) ulinux_cpu2be64(v)
#endif
