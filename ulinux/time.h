#ifndef ULINUX_TIME_H
#define ULINUX_TIME_H
/*******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
*******************************************************************************/
struct ulinux_timespec{
  ulinux_l sec;/*seconds*/
  ulinux_l nsec;/*nanoseconds*/
};

struct ulinux_timeval{
  ulinux_l sec;/*seconds*/
  ulinux_l usec;/*micro seconds, type can be arch dependent*/
};
#endif
