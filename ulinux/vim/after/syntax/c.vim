" Vim syntax file
" Language:		C
" Maintainer:	Sylvain BERTRAND <sylvain.bertrand@gmail.com>
" Last Change:	2011 Nov 26

"compiler types
syn keyword	cType		ulinux_ut ulinux_us ulinux_u ulinux_ul
syn keyword	cType		ulinux_ull
syn keyword	cType		ulinux_t ulinux_s ulinux_i ulinux_l ulinux_ll
syn keyword	cType		ulinux_f

"arch types
syn keyword	cType		ulinux_u8 ulinux_u16 ulinux_u32 ulinux_u64
syn keyword	cType		ulinux_s8 ulinux_s16 ulinux_s32 ulinux_s64
syn keyword	cType		ulinux_f32

syn keyword	cType		ulinux_sz ulinux_ptrdiff

"ulinux namespaced types
syn keyword	cType		ut us u ul
syn keyword	cType		ull
syn keyword	cType		t s i l ll c
syn keyword	cType		u8 u16 u32 u64
syn keyword	cType		s8 s16 s32 s64
syn keyword	cType		f32

"ulinux flow control
syn keyword	cRepeat		loop
syn keyword	cRepeat		ulinux_loop

