#ifndef ULINUX_TYPES_H
#define ULINUX_TYPES_H
/*******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
*******************************************************************************/
#include <ulinux/arch/types.h>

/*
typedef ulinux_t  ulinux_s8;
typedef ulinux_ut ulinux_u8;

typedef ulinux_s  ulinux_s16;
typedef ulinux_us ulinux_u16;

typedef ulinux_i  ulinux_s32;
typedef ulinux_u  ulinux_u32;
typedef ulinux_f  ulinux_f32;

typedef ulinux_ll  ulinux_s64;
typedef ulinux_ull ulinux_u64;

#if BITS_PER_LONG==64
typedef ulinux_ul ulinux_sz;
typedef ulinux_l  ulinux_ptrdiff;
#else
typedef ulinux_u ulinux_sz;
typedef ulinux_i ulinux_ptrdiff;
#endif
*/

#define ulinux_s8 ulinux_t
#define ulinux_u8 ulinux_ut

#define ulinux_s16 ulinux_s
#define ulinux_u16 ulinux_us

#define ulinux_s32 ulinux_i
#define ulinux_u32 ulinux_u
#define ulinux_f32 ulinux_f

#define ulinux_s64 ulinux_ll
#define ulinux_u64 ulinux_ull

#if BITS_PER_LONG==64
#  define ulinux_sz ulinux_ul
#  define ulinux_ptrdiff ulinux_l
#else
#  define ulinux_sz ulinux_u
#  define ulinux_ptrdiff ulinux_i
#endif
#endif
