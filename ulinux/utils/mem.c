/*******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
*******************************************************************************/
#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
#include <stddef.h>

void ulinux_memcpy(ulinux_u8 *d,ulinux_u8 *s,ulinux_u64 len)
{
  while(len--) *d++=*s++;
}

void ulinux_memset(ulinux_u8 *d,ulinux_u8 c,ulinux_u64 len)
{
  while(len--) *d++=c;
}

/*
stolen from linux
*/
ulinux_s8 ulinux_memcmp(ulinux_u8 *d,ulinux_u8 *c,ulinux_u64 len)
{
  const ulinux_u8 *su1,*su2;
  ulinux_i res=0;

  for(su1=d,su2=c;0<len;++su1,++su2,len--) if((res=*su1-*su2)!=0) break;
  return res;
}
