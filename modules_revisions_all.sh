#Those are the modules needed to find our root filesystem can be. For a live
#system, this should be most hardware block drivers without forgetting usb
#mass storage (may need usb attached scsi)
HW_MODULES=ahci
#those modules are "disk" drivers which uses their respective "bus" drivers
#(sd_mod->scsi, usb-storaye->usb...)
DISK_MODULES=sd_mod,usb-storage
#the modules of filesystems we want to include in our initramfs
#WARNING:the soft dep of ext4 on crypto crc32c is not handled, you must add the
#module which registers the crc32c algo manually
FS_MODULES=ext4
