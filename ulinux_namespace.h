#ifndef ULINUX_NAMESPACE_H
#define ULINUX_NAMESPACE_H
#define rt_sigprocmask(a,b,c,d) ulinux_sysc(rt_sigprocmask,4,a,b,c,d)
#define SIG_BLOCK               ULINUX_SIG_BLOCK
#define SIG_UNBLOCK             ULINUX_SIG_UNBLOCK
#define exit_group(a)           ulinux_sysc(exit_group,1,a)
#define MS_NOATIME              ULINUX_MS_NOATIME
#define MS_NODIRATIME           ULINUX_MS_NODIRATIME
#define MS_MOVE                 ULINUX_MS_MOVE
#define mount(a,b,c,d,e)        ulinux_sysc(mount,5,a,b,c,d,e)
#define umount(a)               ulinux_sysc(umount2,2,a,0)
#define chdir(a)                ulinux_sysc(chdir,1,a)
#define chroot(a)               ulinux_sysc(chroot,1,a)
#define ISERR                   ULINUX_ISERR
#define execve(a)               ulinux_sysc(execve,4,a,0,0,0)
#define RDONLY                  ULINUX_O_RDONLY
#define mmap(a,b,c,d,e)       ulinux_sysc(mmap,6,a,b,c,d,e,0)
#define PROT_READ               ULINUX_PROT_READ
#define close(a)                ulinux_sysc(close,1,a)
#define EINTR                   ULINUX_EINTR
/* avoid namespace conflict with libmod header dependencies */
#ifndef LIBKMOD_H
    #define S_IFBLK                 ULINUX_S_IFBLK
    #define S_IRUSR                 ULINUX_S_IRUSR
    #define S_IWUSR                 ULINUX_S_IWUSR
    #define S_IRGRP                 ULINUX_S_IRGRP
    #define S_IWGRP                 ULINUX_S_IWGRP
    #define AT_REMOVEDIR            ULINUX_AT_REMOVEDIR
    #define openat(a,b,c)           ulinux_sysc(openat,4,a,b,c,0)
    #define open(a,b)               ulinux_sysc(open,3,a,b,0)
#else
    #define ul_openat(a,b,c)           ulinux_sysc(openat,4,a,b,c,0)
#endif
#define memcmp(a,b,c)           ulinux_memcmp((ulinux_u8*)(a),(ulinux_u8*)(b),\
c)
#define memset(a,b,c)           ulinux_memset((ulinux_u8*)(a),b,c)
#define dec2u16                 ulinux_dec2u16
#define dec2u32                 ulinux_dec2u32
#define epoll_create1(a)        ulinux_sysc(epoll_create1,1,a)
#define socket(a,b,c)           ulinux_sysc(socket,3,a,b,c)
#define PF_NETLINK              ULINUX_PF_NETLINK
#define SOCK_RAW                ULINUX_SOCK_RAW
#define NETLINK_KOBJECT_UEVENT  ULINUX_NETLINK_KOBJECT_UEVENT
#define setsockopt(a,b,c,d,e)   ulinux_sysc(setsockopt,5,a,b,c,d,e)
#define SOL_SOCKET              ULINUX_SOL_SOCKET
#define SO_RCVBUFFORCE          ULINUX_SO_RCVBUFFORCE
#define sockaddr_nl             ulinux_sockaddr_nl
#define AF_NETLINK              ULINUX_AF_NETLINK
#define bind(a,b,c)             ulinux_sysc(bind,3,a,b,c)
#define epoll_ctl(a,b,c,d)      ulinux_sysc(epoll_ctl,4,a,b,c,d)
#define EPOLLIN                 ULINUX_EPOLLIN
#define EAGAIN                  ULINUX_EAGAIN
#define EPOLL_CTL_ADD           ULINUX_EPOLL_CTL_ADD
#define io_vec                  ulinux_io_vec
#define msg_hdr                 ulinux_msg_hdr
#define recvmsg(a,b,c)          ulinux_sysc(recvmsg,3,a,b,c)
#define MSG_TRUNC               ULINUX_MSG_TRUNC
#define epoll_event             ulinux_epoll_event
#define epoll_wait(a,b,c,d)     ulinux_sysc(epoll_wait,4,a,b,c,d)
#define strcat                  ulinux_strcat
#define strncmp(a,b,c)          ulinux_strncmp((ulinux_u8*)(a),(ulinux_u8*)(b),(ulinux_u64)(c))
#define strcpy(a,b)             ulinux_strcpy((ulinux_u8*)(a),(ulinux_u8*)(b))
#define strcmp(a,b)             ulinux_strcmp((ulinux_u8*)(a),(ulinux_u8*)(b))
#define unlinkat(a,b,c)         ulinux_sysc(unlinkat,3,a,b,c)
#define dirent64                ulinux_dirent64
#define DT_DIR                  ULINUX_DT_DIR
#define NONBLOCK                ULINUX_O_NONBLOCK
#define getdents64(a,b,c)       ulinux_sysc(getdents64,3,a,b,c)
#define read(a,b,c)             ulinux_sysc(read,3,a,b,c)
#define PAGE_SZ			ULINUX_PAGE_SZ
#define u8                      ulinux_u8
#define s8                      ulinux_s8
#define u16                     ulinux_u16
#define u32                     ulinux_u32
#define i                       int
#define c                       char
#define l                       long
#define ul                      unsigned long
#define loop                    while(1)
#endif
