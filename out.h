#ifndef OUT_H
#define OUT_H
/*******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
*******************************************************************************/
#ifndef QUIET
#include <stdarg.h>
#include <ulinux/utils/ascii/string/vsprintf.h>

#define PRE "initramfs:"

#ifdef INIT_C
  ulinux_u8 *g_dprintf_buf;
#else
  extern ulinux_u8 *g_dprintf_buf;
#endif

#define DPRINTF_BUF_SZ 1024
#define OUT(f,...) ulinux_dprintf(0,g_dprintf_buf,DPRINTF_BUF_SZ,\
(ulinux_u8*)(f),##__VA_ARGS__)
#else
#define PRE
#define OUT(f,...)
#endif
#endif
