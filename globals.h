/*******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
*******************************************************************************/

#ifdef INIT_C
  #define EXTERN
#else
  #define EXTERN extern
#endif

EXTERN u8 *root_uuid;
#define ROOT_DEV_PATH_SZ 256*2
#define ROOT_FS_TYPE_SZ 256
EXTERN u8 root_dev_path[ROOT_DEV_PATH_SZ];
EXTERN u8 root_fs_type[ROOT_FS_TYPE_SZ];

/*some shared returned values*/
#define ROOT_FOUND     1
#define ROOT_NOT_FOUND 0
