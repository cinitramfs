#!/bin/sh

echo "\
#ifndef STATIC_MODULES_H
#define STATIC_MODULES_H
ulinux_u8 *static_modules[]={"
IFS=","
for m in $*; do
    echo "(ulinux_u8*)\"$m\","
done
echo "\
0};
#endif"

