#ifndef MODULES_H
#define MODULES_H
/*******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
*******************************************************************************/
extern void modules_setup(void);
extern void modules_probe_static(void);
extern void modules_probe_drivers(void);
extern void modules_probe_name(u8 *name);
extern void modules_cleanup(void);
#endif
