#ifndef UEVENT_H
#define UEVENT_H
/*******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
*******************************************************************************/
extern u8 uevent_process(u8 *p,i sz);
#endif
