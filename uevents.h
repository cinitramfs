#ifndef UEVENTS_H
#define UEVENTS_H
/*******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
*******************************************************************************/
extern void uevents_setup(void);
extern void uevents_cleanup(void);
extern u8 uevents_process(void);
#endif
